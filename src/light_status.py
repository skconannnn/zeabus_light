import rospy
from modbus_ascii_ros.srv import IOCommand
import numpy as np
global srv_name

class light_status :
    def __init__(self):
        rospy.init_node('service_light', annonymous=True)
        print 'init node'
    
    def run(self, channel, cmd):
        if cmd == 'on' :
            srv_name = "/io_and_pressure/IO_ON"
        else :
            srv_name = "/io_and_pressure/IO_OFF"

        rospy.wait_for_service(srv_name)
        srv = rospy.ServiceProxy(srv_name,IOCommand)
        result = srv(channel)   
        print result
        
if __name__=='__main__': 	
    light = light_status()
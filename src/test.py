import rospy
from modbus_ascii_ros.srv import IOCommand
import numpy as np
global srv_name
if __name__=='__main__':
    rospy.init_node('service_light')
    on = "/io_and_pressure/IO_ON"
    off = "/io_and_pressure/IO_OFF"
    srv_name=on
    print 'service start'
    light = [7,8,9,10,11]
    ch = np.int32(7)
    time = 0.5 
    for i in range (10000):
     if i%2 == 0:
      srv_name = on
     else :
      srv_name = off
     rospy.wait_for_service(srv_name)
     srv = rospy.ServiceProxy(srv_name,IOCommand)
     ch = light[(i/2)%5]
     result = srv(ch)
     print result
     time = 0.2
     if i%2==0 :
      time = 0.6
     rospy.sleep(time)
     
    	
